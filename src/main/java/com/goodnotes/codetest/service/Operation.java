package com.goodnotes.codetest.service;

import com.goodnotes.codetest.dto.Tree;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface Operation {

  void perform(Tree tree, int value, boolean notify);

}
