package com.goodnotes.codetest.service.impl;

import com.goodnotes.codetest.dto.Node;
import com.goodnotes.codetest.dto.Tree;
import com.goodnotes.codetest.service.Search;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class Query implements Search {

  /**
   * Query for Children of a Node({N})
   * @param value - int value of Node to found
   * @return List<Tree> - The List (Left, Right) Children
   */
  @Override
  public List<Node> children(Tree tree, int value) {
    if (value == tree.getHead().data)
      return getChildren(tree.getHead());

    Node left = this.findLeft(tree.getHead(), value);
    if (left != null)
      return getChildren(left);

    Node right = this.findRight(tree.getHead(), value);
    if (right != null)
      return getChildren(right);

    return null;
  }

  /**
   * Helper method to retrieve List Children of a Node({N})
   * @param current - The Tree current Node({N})
   * @return List<Tree> - List of Node Children
   */
  private List<Node> getChildren(Node current) {
    if (current == null) return null;

    List<Node> children = new ArrayList<>();

    if (current.left != null)
      children.add(current.left);

    if (current.right != null)
      children.add(current.right);

    return children;
  }

  /**
   * using recursion
   * Helper method to iterate Left of Tree List and find Node({N})
   * @param current - Tree current starts with head
   * @param value - int value to find
   * @return Tree - The Node Tree with Children
   */
  private Node findLeft(Node current, int value) {
    while (current.left != null) {
      if (current.left.data == value)
        return current.left;

      Node found = this.findRight(current.left, value);
      if (found != null) return found;

      current = current.left;
    }
    return null;
  }

  /**
   * using recursion
   * Helper method to iterate Right of Tree List and find Node({N})
   * @param current - Tree current starts with head
   * @param value - int value to find
   * @return Tree - The Node Tree with Children
   */
  private Node findRight(Node current, int value) {
    while (current.right != null) {
      if (current.right.data == value)
        return current.right;

      Node found = this.findLeft(current.right, value);
      if (found != null) return found;

      current = current.right;
    }
    return null;
  }
}
