package com.goodnotes.codetest.service.impl;

import com.goodnotes.codetest.dto.Node;
import com.goodnotes.codetest.dto.Tree;
import com.goodnotes.codetest.event.EventType;
import com.goodnotes.codetest.service.Operation;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class Remove implements Operation {

  private Add operation;

  public Remove(Add operation) {
    this.operation = operation;
  }

  /**
   * Remove a Node of Tree based on it's value
   * @param tree - the current Tree
   * @param value - int value to be removed
   * @param notify - boolean notify informs if the notification needs be invoked
   */
  @Override
  public void perform(Tree tree, int value, boolean notify) {

    if (tree.getHead() == null) return;

    Node current = tree.getHead();

    if (current.data == value) {
      tree.setHead(null);
      operation.iterateAddLeft(tree, current);
      operation.iterateAddRight(tree, current);
    }

    if (value < current.data) {
      Node temp = this.removeLeft(current, value);
      operation.iterateAddLeft(tree, temp);
    }

    if (value > current.data) {
      Node temp = this.removeRight(current, value);
      operation.iterateAddRight(tree, temp);
    }

    if (notify) {
      tree.notify(EventType.REMOVE, value);
      tree.notify(EventType.REMOVE_REPLICA, value);
    }

  }

  /**
   * Iterates Left of Tree, check if current Left is equals the value to be removed
   * Depends on the check, the Node will be set to null
   * @param current - Node - the current Node
   * @param value - int value to be added
   * @return Node - the Node removed used to iterate Left and Right of it
   */
  private Node removeLeft(Node current, int value) {
    while (current.left != null) {
      if(value == current.left.data){
        Node temp = current.left;
        current.left = null;
        return temp;
      }
      current = current.left;
    }
    return null;
  }

  /**
   * Iterates Right of Tree, check if current Right is equals the value to be removed
   * Depends on the check, the Node will be set to null
   * @param current - Node - the current Node
   * @param value - int value to be added
   * @return Node - the Node removed used to iterate Left and Right of it
   */
  private Node removeRight(Node current, int value) {
    while (current.right != null) {
      if(value == current.right.data){
        Node temp = current.right;
        current.right = null;
        return temp;
      }
      current = current.right;
    }
    return null;
  }

}
