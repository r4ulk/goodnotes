package com.goodnotes.codetest.service.impl;

import com.goodnotes.codetest.dto.Node;
import com.goodnotes.codetest.dto.Tree;
import com.goodnotes.codetest.event.EventManager;
import com.goodnotes.codetest.event.EventType;
import com.goodnotes.codetest.service.Operation;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class Add implements Operation {

  /**
   * ADD {N} in the Tree
   * @param tree - the current Tree
   * @param value - the int value to be added
   * @param notify - boolean notify informs if the notification needs be invoked
   */
  @Override
  public void perform(Tree tree, int value, boolean notify) {

    EventManager events = tree.getEvents();

    // initializes Tree
    if (tree.getHead() == null) {
      tree.setHead(value);

      if(notify)
        events.notify(EventType.ADD, value);

      return;
    }

    Node current = tree.getHead();

    // Check current value to be added and skip it if equals the head data
    if (value == current.data) return;

    if(value < current.data)
      current = addLeft(current, value);

    this.addRight(current, value);

    if (notify) {
      tree.notify(EventType.ADD_REPLICA, value);
      tree.notify(EventType.ADD, value);
    }

    return;
  }

  /**
   * Iterates Left of Tree, check if {N} is greater than current Node value or not
   * Depends on the check, the value will be added Left or Right using recursion
   * @param current - Node - the current Node
   * @param value - int value to be added
   * @return Node - the current ADDED Node
   */
  private Node addLeft(Node current, int value) {
    // Minor Values
    while (current.left != null) {
      if(value > current.data)
        // recursion for add on right side
        return this.addRight(current, value);

      current = current.left;
    }

    if(value < current.data)
      current.left = new Node(value);

    if(value > current.data)
      current.right = new Node(value);

    return current;
  }

  /**
   * Iterates Right of Tree check if {N} is less than current node value or not
   * Depends on the check, the value will be added Left or Right using recursion
   * @param current - Node - the current Node
   * @param value - int value to be added
   * @return Node - the current ADDED Node
   */
  private Node addRight(Node current, int value) {
    // Major Values
    while (current.right != null) {
      if(value < current.data)
        // recursion for add on left side
        return this.addLeft(current, value);

      current = current.right;
    }

    if(value < current.data)
      current.left = new Node(value);

    if(value > current.data)
      current.right = new Node(value);

    return current;
  }

  /**
   * using recursion
   * Helper method to iterate Left of a Tree and uses this.add(N) to composite the Three
   * @param current - Tree current Node to starts iteration
   */
  public void iterateAddLeft(Tree tree, Node current) {
    while (current.left != null) {
      tree.add(current.left.data);
      this.iterateAddRight(tree, current.left);
      current = current.left;
    }
  }


  /**
   * using recursion
   * Helper method to iterate Right of a Tree and uses this.add(N) to composite the Three List
   * @param current - Tree current Node to starts iteration
   */
  public void iterateAddRight(Tree tree, Node current) {
    while (current.right != null) {
      tree.add(current.right.data);
      this.iterateAddLeft(tree, current.right);
      current = current.right;
    }
  }

}
