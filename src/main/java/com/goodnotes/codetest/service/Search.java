package com.goodnotes.codetest.service;

import com.goodnotes.codetest.dto.Node;
import com.goodnotes.codetest.dto.Tree;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
import java.util.List;

public interface Search {

  List<Node> children(Tree tree, int value);

}
