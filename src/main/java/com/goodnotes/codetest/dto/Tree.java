package com.goodnotes.codetest.dto;

import com.goodnotes.codetest.event.EventListener;
import com.goodnotes.codetest.event.EventManager;
import com.goodnotes.codetest.event.EventType;
import com.goodnotes.codetest.service.Operation;
import com.goodnotes.codetest.service.Search;
import com.goodnotes.codetest.service.impl.Add;
import com.goodnotes.codetest.service.impl.Query;
import com.goodnotes.codetest.service.impl.Remove;

import java.util.List;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class Tree implements EventListener {

	private EventManager events;

	private Node head;

	private Operation add = new Add();

	private Operation remove = new Remove(new Add());

	private Search search = new Query();

	public Tree() {
		this.events = new EventManager(EventType.ADD, EventType.REMOVE);
	}

	public void setHead(Integer value) {
		if(value == null) {
			this.head = null;
			return;
		}
		this.head = new Node(value);
	}

	public Node getHead() {
		return this.head;
	}

	public void setEvents(EventType... events) {
		this.events = new EventManager(events);
	}

	public EventManager getEvents() {
		return this.events;
	}

	@Override
	public void emmit(EventType eventType, int value) {
		System.out.println("Has performed: [ " + eventType + " ] operation with the following value: " + value);
		if (eventType.equals(EventType.ADD))
			add.perform(this, value, Boolean.FALSE);

		if (eventType.equals(EventType.REMOVE))
			remove.perform(this, value, Boolean.FALSE);

		if (eventType.equals(EventType.ADD_REPLICA))
			this.add(value);

		if (eventType.equals(EventType.REMOVE_REPLICA))
			this.remove(value);

	}

	public void setReplica(Tree replica) {
		// ADD replica AS subscriber OF master
		this.events.subscribe(EventType.ADD, replica);
		this.events.subscribe(EventType.REMOVE, replica);

		// ADD master AS subscriber OF replica
		replica.events = new EventManager(EventType.ADD_REPLICA, EventType.REMOVE_REPLICA);
		replica.events.subscribe(EventType.ADD_REPLICA, this);
		replica.events.subscribe(EventType.REMOVE_REPLICA, this);
	}

	public void add(int value) {
		add.perform(this, value, Boolean.TRUE);
	}

	public void remove(int value) {
		remove.perform(this, value, Boolean.TRUE);
	}

	public List<Node> getChildren(int value) {
		return search.children(this, value);
	}

	public Node root() {
		return this.head;
	}

	public void notify(EventType eventType, int value) {
		if (events.getListeners().containsKey(eventType.getValue())
					&& !events.getListeners().get(eventType.getValue()).isEmpty())
			events.notify(eventType, value);
	}

}
