package com.goodnotes.codetest.dto;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class Node {
      
	public int data;
	public Node left;
	public Node right;

	public Node(int val) {
		this.data = val;
	}
	
}
