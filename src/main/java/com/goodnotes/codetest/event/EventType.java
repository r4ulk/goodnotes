package com.goodnotes.codetest.event;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public enum EventType {
  ADD("add"),
  REMOVE("remove"),
  ADD_REPLICA("add_replica"),
  REMOVE_REPLICA("remove_replica");

  private String value = null;

  EventType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

}
