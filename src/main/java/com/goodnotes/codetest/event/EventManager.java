package com.goodnotes.codetest.event;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public class EventManager {
  private Map<String, List<EventListener>> listeners = new HashMap<>();

  public Map<String, List<EventListener>> getListeners() {
    return this.listeners;
  }

  public EventManager(EventType... operations) {
    for (EventType operation : operations) {
      this.listeners.put(operation.getValue(), new ArrayList<>());
    }
  }

  public void subscribe(EventType eventType, EventListener listener) {
    List<EventListener> users = listeners.get(eventType.getValue());
    users.add(listener);
  }

  public void unsubscribe(EventType eventType, EventListener listener) {
    List<EventListener> users = listeners.get(eventType.getValue());
    users.remove(listener);
  }

  public void notify(EventType eventType, int value) {
    List<EventListener> services = listeners.get(eventType.getValue());
    for (EventListener listener : services) {
      listener.emmit(eventType, value);
    }
  }
}
