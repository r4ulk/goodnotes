package com.goodnotes.codetest.event;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
public interface EventListener {
  void emmit(EventType eventType, int value);
}
