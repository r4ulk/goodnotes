package com.goodnotes.codetest.dto;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TreeTest {

  /***************************
   *  Should ADD Node(7)
   *
   *          7
   *         / \
   *     null   null
   ***************************/
  @Test
  public void shouldADDTest() {
    Tree tree = new Tree();
    tree.add(7);

    assertThat(tree).isNotNull();
    assertThat(tree.getHead()).isNotNull();
    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNull();
    assertThat(tree.getHead().right).isNull();
  }

  /********************************
   *  Should ADD Node(7) with:
   *    LEFT Node(5)
   *
   *          7
   *         / \
   *       5   null
   ********************************/
  @Test
  public void shouldADDMinorValueTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(5);

    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNotNull();
    assertThat(tree.getHead().right).isNull();
    assertThat(tree.getHead().left.data).isEqualTo(5);
  }

  /*******************************
   *  Should ADD Node(7) with:
   *    LEFT Node(5) with:
   *      LEFT Node(3)
   *
   *           7
   *          / \
   *        5    null
   *      /  \   /  \
   *     3   null   null
   *   /  \         / \
   * null null   null  null
   *******************************/
  @Test
  public void shouldADDTwoMinorValuesTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(5);
    tree.add(3);

    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNotNull();
    assertThat(tree.getHead().right).isNull();
    assertThat(tree.getHead().left.data).isEqualTo(5);
    assertThat(tree.getHead().left.left).isNotNull();
    assertThat(tree.getHead().left.left.data).isEqualTo(3);
  }

  /*******************************
   *  Should ADD Node(7) with:
   *    RIGHT Node(9)
   *
   *          7
   *         / \
   *     null   9
   *******************************/
  @Test
  public void shouldADDGreaterValuesTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);

    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNull();
    assertThat(tree.getHead().right).isNotNull();
    assertThat(tree.getHead().right.data).isEqualTo(9);
  }

  /*******************************
   *  Should ADD Node(7) with:
   *    RIGHT Node(9) with:
   *      RIGHT Node(11)
   *
   *          7
   *         / \
   *     null   9
   *           / \
   *       null   11
   *             / \
   *         null   null
   *******************************/
  @Test
  public void shouldADDTwoGreaterTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);
    tree.add(11);

    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNull();
    assertThat(tree.getHead().right).isNotNull();
    assertThat(tree.getHead().right.data).isEqualTo(9);
    assertThat(tree.getHead().right.right).isNotNull();
    assertThat(tree.getHead().right.right.data).isEqualTo(11);
  }

  /***********************
   *  Should ADD Node(7) with:
   *    LEFT Node(5) with:
   *      LEFT Node(1) with:
   *        RIGHT Node(3)
   *    RIGHT Node(9) with:
   *      RIGHT Node(11)
   *
   *            7
   *          /  \
   *        5     9
   *      /  \   /  \
   *     1    null    11
   *   /  \          / \
   * null  3     null  null
   ***********************/
  @Test
  public void shouldADDGreaterAndMinorValuesTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);
    tree.add(5);
    tree.add(11);
    tree.add(1);
    tree.add(3);

    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNotNull();
    assertThat(tree.getHead().left.data).isEqualTo(5);
    assertThat(tree.getHead().left.left).isNotNull();
    assertThat(tree.getHead().left.left.right.data).isEqualTo(3);

    assertThat(tree.getHead().right).isNotNull();
    assertThat(tree.getHead().right.data).isEqualTo(9);
    assertThat(tree.getHead().right.right).isNotNull();
    assertThat(tree.getHead().right.right.data).isEqualTo(11);

  }

  /***********************************************************************************
   *  Should REMOVE Node(7) from Tree:
   *
   *            7    =>   Node(7) will be removed and Head will be Node (5)
   *          /  \                                                     /    \
   *        5     9                                                  1       9
   *      /  \   /  \                                              /  \     /  \
   *     1    null   null                                      null  null null null
   ************************************************************************************/
  @Test
  public void shouldREMOVENodeTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);
    tree.add(5);
    tree.add(1);

    // remove
    tree.remove(7);

    assertThat(tree.getHead().data).isEqualTo(5);
    assertThat(tree.getHead().left).isNotNull();
    assertThat(tree.getHead().left.data).isEqualTo(1);

    assertThat(tree.getHead().right).isNotNull();
    assertThat(tree.getHead().right.data).isEqualTo(9);

  }

  /**********************************************************************
   * Should REMOVE Node(5) from Tree then:
   *  Tree needs to be reordered by left position always ( rule that I assumed to rebuild the Tree )
   *
   *            7                                              7
   *          /  \                                          /    \
   *       (5)     9               =>                     (1)     9
   *      /  \   /  \                                    /  \    /  \
   *     1    null   null                             null null null null
   ************************************************************************/
  @Test
  public void shouldREMOVENodeScenaryTwoTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);
    tree.add(5);
    tree.add(1);

    // remove
    tree.remove(5);

    assertThat(tree.getHead().data).isEqualTo(7);
    assertThat(tree.getHead().left).isNotNull();
    assertThat(tree.getHead().left.data).isEqualTo(1);

    assertThat(tree.getHead().right).isNotNull();
    assertThat(tree.getHead().right.data).isEqualTo(9);

  }

  /**********************************************
   *  Should Query for the ROOT level of Tree
   *
   *            7
   *          /  \
   *        5     9
   *      /  \   /  \
   *     1    null    11
   *   /  \          / \
   * null  3     null  null
   ***********************************************/
  @Test
  public void shouldQUERYForROOTTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);
    tree.add(5);
    tree.add(11);
    tree.add(1);
    tree.add(3);

    Node root = tree.root();

    assertThat(root.data).isEqualTo(7);

  }

  /***********************
   *  Query for Children of a Node.
   *   As the Node has LEFT and RIGHT sides
   *    The implementation will return a List<Node> of max size equals 2 (LEFT, RIGHT)
   *
   *            7
   *          /  \
   *        5     9
   *      /  \   /  \
   *     1    null    11
   *   /  \          / \
   * null  3     null  null
   ***********************/
  @Test
  public void shouldQUERYForChildrenOfNodeTest() {
    Tree tree = new Tree();
    tree.add(7);
    tree.add(9);
    tree.add(5);
    tree.add(11);
    tree.add(1);
    tree.add(3);

    List<Node> children = tree.getChildren(9);

    assertThat(children).isNotEmpty();
    assertThat(children.size()).isEqualTo(1);

    Node node = children.get(0);

    assertThat(node.data).isEqualTo(11);

  }

  /**********************************************************
   * Should ADD & Replicate From Master -> to Replica
   *
   *        MASTER TREE                       REPLICA TREE
   *            7                                  7
   *          /  \                               /   \
   *       null  null                         null   null
   ***********************************************************/
  @Test
  public void shouldREPLICATESceneryOneTest() {
    Tree master  = new Tree();
    Tree replica = new Tree();

    master.setReplica(replica);

    master.add(7);

    assertThat(master.getHead()).isNotNull();
    assertThat(replica.getHead()).isNotNull();

    assertThat(master.getHead().data).isEqualTo(7);
    assertThat(replica.getHead().data).isEqualTo(7);
  }

  /*****************************************************************
   * Should ADD Two Nodes & Replicate From Master -> to Replica
   *
   *        MASTER TREE                       REPLICA TREE
   *            5                                  5
   *          /  \                               /   \
   *         1  null                            1   null
   *       /  \                               /  \
   *    null  null                         null  null
   *****************************************************************/
  @Test
  public void shouldREPLICATESceneryTwoTest() {
    Tree master = new Tree();
    Tree replica = new Tree();

    master.setReplica(replica);

    master.add(5);
    master.add(1);

    assertThat(master.getHead().left).isNotNull();
    assertThat(master.getHead().left.data).isEqualTo(1);

    assertThat(replica.getHead().left).isNotNull();
    assertThat(replica.getHead().left.data).isEqualTo(1);
  }

  /*******************************************************************
   * Should ADD Three Nodes & Replicate From Master -> to Replica
   *
   *        MASTER TREE                       REPLICA TREE
   *            5                                  5
   *          /  \                               /   \
   *         1  null                            1   null
   *       /  \                               /  \
   *    null   3                           null   3
   *         /  \                               /  \
   *      null  null                         null  null
   *******************************************************************/
  @Test
  public void shouldREPLICATESceneryThreeTest() {
    Tree master = new Tree();
    Tree replica = new Tree();

    master.setReplica(replica);

    master.add(5);
    master.add(1);
    master.add(3);

    assertThat(master.getHead().left).isNotNull();
    assertThat(master.getHead().left.data).isEqualTo(1);

    assertThat(replica.getHead().left).isNotNull();
    assertThat(replica.getHead().left.data).isEqualTo(1);
  }

  /**********************************************************************
   * Should Build Complex Tree & Replicate From Master -> to Replica
   *
   *        MASTER TREE                       REPLICA TREE
   *             5                                  5
   *          /    \                             /    \
   *        2       9                          2       9
   *      /  \     /  \                      /  \     /  \
   *     1    3  7    11                    1   3    7   11
   *        /  \                              /  \
   *     null   null                      null   null
   ************************************************************************/
  @Test
  public void shouldADDReplicateSceneryFourTest() {
    Tree master = new Tree();
    Tree replica = new Tree();

    master.setReplica(replica);

    // Add 5
    master.add(5);
    Node masterNode  = master.getHead();
    Node replicaNode = master.getHead();

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(5);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(5);

    // Add 2
    master.add(2);
    masterNode  = masterNode.left;
    replicaNode = replicaNode.left;

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(2);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(2);

    // Add 3
    master.add(3);
    masterNode  = masterNode.right;
    replicaNode = replicaNode.right;

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(3);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(3);

    // Add 9
    master.add(9);
    masterNode  = master.getHead().right;
    replicaNode = replica.getHead().right;

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(9);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(9);

    // Add 7
    master.add(7);
    masterNode  = masterNode.left;
    replicaNode = replicaNode.left;

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(7);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(7);

    // Add 1
    master.add(1);
    masterNode  = master.getHead().left.left;
    replicaNode = replica.getHead().left.left;

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(1);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(1);

    // Add 11
    master.add(11);
    masterNode  = master.getHead().right.right;
    replicaNode = replica.getHead().right.right;

    // Test Master
    assertThat(masterNode).isNotNull();
    assertThat(masterNode.data).isEqualTo(11);

    // Test Replica
    assertThat(replicaNode).isNotNull();
    assertThat(replicaNode.data).isEqualTo(11);


  }

  /**********************************************************
   * Should ADD & Replicate From Master -> to Replica
   *    && ADD From Replica -> to Master
   *
   *  Test if both are MERGED after updates
   *
   *        MASTER TREE                       REPLICA TREE
   *            7                                  7
   *          /  \                               /   \
   *        5  null                             5   null
   **********************************************************/
  @Test
  public void shouldADDReplicateFromMasterAndFromReplicaTest() {
    Tree master  = new Tree();
    Tree replica = new Tree();

    master.setReplica(replica);

    // Add 7 to MASTER
    master.add(7);

    // Test Master
    assertThat(master.getHead()).isNotNull();
    assertThat(master.getHead().data).isEqualTo(7);

    // Test Replica
    assertThat(replica.getHead()).isNotNull();
    assertThat(replica.getHead().data).isEqualTo(7);

    // Add 5 to REPLICA
    replica.add(5);

    // Test Master
    assertThat(master.getHead().left).isNotNull();
    assertThat(master.getHead().left.data).isEqualTo(5);

    // Test Replica
    assertThat(replica.getHead().left).isNotNull();
    assertThat(replica.getHead().left.data).isEqualTo(5);

  }

  /****************************************************************
   *  Should ADD & Replicate From Master -> to Replica(s) 1
   *    && ADD From Replica 1 -> to Master
   *      && ADD From Replica 2 -> to Master -> to Replica 1
   *
   *  Test if both are MERGED after updates
   *
   *         MASTER               REPLICA 1             REPLICA 2
   *            7                    7                    7
   *          /  \                 /   \                /   \
   *        5     9               5     9              5     9
   ***************************************************************/
  @Test
  public void shouldADDReplicateFromMasterAndFromReplicasTest() {
    Tree master  = new Tree();
    Tree replica1 = new Tree();
    Tree replica2 = new Tree();

    master.setReplica(replica1);
    master.setReplica(replica2);

    // Add 7 to MASTER
    master.add(7);

    // Test Master
    assertThat(master.getHead()).isNotNull();
    assertThat(master.getHead().data).isEqualTo(7);

    // Test Replica 1
    assertThat(replica1.getHead()).isNotNull();
    assertThat(replica1.getHead().data).isEqualTo(7);

    // Test Replica 2
    assertThat(replica2.getHead()).isNotNull();
    assertThat(replica2.getHead().data).isEqualTo(7);

    // Add 5 to REPLICA 1
    replica1.add(5);

    // Test Master
    assertThat(master.getHead().left).isNotNull();
    assertThat(master.getHead().left.data).isEqualTo(5);

    // Test Replica 1
    assertThat(replica1.getHead().left).isNotNull();
    assertThat(replica1.getHead().left.data).isEqualTo(5);

    // Test Replica 2
    assertThat(replica2.getHead().left).isNotNull();
    assertThat(replica2.getHead().left.data).isEqualTo(5);

    // Add 9 to REPLICA 2
    replica2.add(9);

    // Test Master
    assertThat(master.getHead().right).isNotNull();
    assertThat(master.getHead().right.data).isEqualTo(9);

    // Test Replica 1
    assertThat(replica1.getHead().right).isNotNull();
    assertThat(replica1.getHead().right.data).isEqualTo(9);

    // Test Replica 2
    assertThat(replica2.getHead().right).isNotNull();
    assertThat(replica2.getHead().right.data).isEqualTo(9);

  }

  /********************************************************************************
   *  Should Add & Replicate From Master -> to Replica(s) 1
   *    && Add From Replica 1 -> to Master
   *      && Add From Replica 2 -> to Master -> to Replica 1
   *
   *  Should REMOVE & Replicate From Master -> to Replica(s) 1 -> to Replica(s) 2

   *  Test if both are MERGED after updates
   *
   *         MASTER               REPLICA 1             REPLICA 2
   *            7                    7                    7
   *          /  \                 /   \                /   \
   *        5    null            5     null            5     null
   **********************************************************************************/
  @Test
  public void shouldREMOVEReplicateFromMasterAndFromReplicasTest() {
    Tree master  = new Tree();
    Tree replica1 = new Tree();
    Tree replica2 = new Tree();

    master.setReplica(replica1);
    master.setReplica(replica2);

    // Add 7 to MASTER
    master.add(7);

    // Add 5 to REPLICA 1
    replica1.add(5);

    // Add 9 to REPLICA 2
    replica2.add(9);

    // REMOVE 9 from MASTER
    master.remove(9);

    // Test Master
    assertThat(master.getHead().right).isNull();

    // Test Replica 1
    assertThat(replica1.getHead().right).isNull();

    // Test Replica 2
    assertThat(replica2.getHead().right).isNull();

  }
}
