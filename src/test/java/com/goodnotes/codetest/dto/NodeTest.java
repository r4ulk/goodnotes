package com.goodnotes.codetest.dto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NodeTest {

  @Test
  public void treeCreationTest() {
    Node node = new Node(7);

    assertThat(node).isNotNull();
    assertThat(node.data).isEqualTo(7);
    assertThat(node.left).isNull();
    assertThat(node.right).isNull();
  }

}
