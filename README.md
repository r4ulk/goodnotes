# README #

This a code challenge of GoodNotes using:

- Spring-Boot (Java 11), 
- Gradle build

### About the challenge ###

Create a CRDT Conflict Free Replicated Data Types Tree

### What I did? ###

- Replication using observability

### Running ###
```
gradle build
```

### Author ###

Raul Klumpp <raulklumpp@gmail.com>

https://www.linkedin.com/in/raulk/